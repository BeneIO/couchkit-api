package io.bene.couchkit.api;

import java.util.function.Consumer;

/**
 * Created by Bene on 26.10.2015.
 */
public interface Result<T> {

    Result onResult(Consumer<T> resultConsumer);

    Result onError(Consumer<Exception> errorConsumer);

}