package io.bene.couchkit.api;

import com.couchbase.client.java.Bucket;

/**
 * Created by Bene on 26.10.2015.
 */
public interface CouchKit {

    Bucket getBucket(String id);
    AsyncCouchKit async();

}