package io.bene.couchkit.api;

import com.couchbase.client.java.Bucket;

/**
 * Created by Bene on 26.10.2015.
 */
public interface AsyncCouchKit {

    Result<Bucket> getBucket(String id);

}